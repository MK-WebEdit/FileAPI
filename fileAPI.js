/** TODO:
 * IOException
 * FileNotFoundException (extends IOException)
 */

// public prototypes
{
	/**
	 * Method that adds to a array, only when its not already in
	 * @param newValue
	 * @return {Array}
	 */
	Array.prototype.pushDistinct = function (newValue) {
		if (this.indexOf(newValue) === -1) {
			this.push(newValue);
		}
		return this;
	};

	/**
	 * Removes one item from the array
	 * @param toRemove
	 * @return {Array}
	 */
	Array.prototype.removeItem = function (toRemove) {
		var index = this.indexOf(toRemove);
		if (index !== -1) {
			this.splice(index, 1);
		}
		return this;
	};

	if (!String.prototype.startsWith) {
		String.prototype.startsWith = function (searchString, position) {
			position = position || 0;
			return this.indexOf(searchString, position) === position;
		};
	}

	if (!String.prototype.endsWith) {
		String.prototype.endsWith = function (searchString, position) {
			position = position || this.length;
			return this.indexOf(searchString, position) === position;
		};

	}
}

/**
 * MD5 lib, modified into MD5 namespace
 * http://www.myersdaily.org/joseph/javascript/md5.js
 */
/** @namespace */
var MD5 = MD5 || {};
{
	MD5.md5cycle = function (x, k) {
		var a = x[0], b = x[1], c = x[2], d = x[3];

		a = MD5.ff(a, b, c, d, k[0], 7, -680876936);
		d = MD5.ff(d, a, b, c, k[1], 12, -389564586);
		c = MD5.ff(c, d, a, b, k[2], 17, 606105819);
		b = MD5.ff(b, c, d, a, k[3], 22, -1044525330);
		a = MD5.ff(a, b, c, d, k[4], 7, -176418897);
		d = MD5.ff(d, a, b, c, k[5], 12, 1200080426);
		c = MD5.ff(c, d, a, b, k[6], 17, -1473231341);
		b = MD5.ff(b, c, d, a, k[7], 22, -45705983);
		a = MD5.ff(a, b, c, d, k[8], 7, 1770035416);
		d = MD5.ff(d, a, b, c, k[9], 12, -1958414417);
		c = MD5.ff(c, d, a, b, k[10], 17, -42063);
		b = MD5.ff(b, c, d, a, k[11], 22, -1990404162);
		a = MD5.ff(a, b, c, d, k[12], 7, 1804603682);
		d = MD5.ff(d, a, b, c, k[13], 12, -40341101);
		c = MD5.ff(c, d, a, b, k[14], 17, -1502002290);
		b = MD5.ff(b, c, d, a, k[15], 22, 1236535329);

		a = MD5.gg(a, b, c, d, k[1], 5, -165796510);
		d = MD5.gg(d, a, b, c, k[6], 9, -1069501632);
		c = MD5.gg(c, d, a, b, k[11], 14, 643717713);
		b = MD5.gg(b, c, d, a, k[0], 20, -373897302);
		a = MD5.gg(a, b, c, d, k[5], 5, -701558691);
		d = MD5.gg(d, a, b, c, k[10], 9, 38016083);
		c = MD5.gg(c, d, a, b, k[15], 14, -660478335);
		b = MD5.gg(b, c, d, a, k[4], 20, -405537848);
		a = MD5.gg(a, b, c, d, k[9], 5, 568446438);
		d = MD5.gg(d, a, b, c, k[14], 9, -1019803690);
		c = MD5.gg(c, d, a, b, k[3], 14, -187363961);
		b = MD5.gg(b, c, d, a, k[8], 20, 1163531501);
		a = MD5.gg(a, b, c, d, k[13], 5, -1444681467);
		d = MD5.gg(d, a, b, c, k[2], 9, -51403784);
		c = MD5.gg(c, d, a, b, k[7], 14, 1735328473);
		b = MD5.gg(b, c, d, a, k[12], 20, -1926607734);

		a = MD5.hh(a, b, c, d, k[5], 4, -378558);
		d = MD5.hh(d, a, b, c, k[8], 11, -2022574463);
		c = MD5.hh(c, d, a, b, k[11], 16, 1839030562);
		b = MD5.hh(b, c, d, a, k[14], 23, -35309556);
		a = MD5.hh(a, b, c, d, k[1], 4, -1530992060);
		d = MD5.hh(d, a, b, c, k[4], 11, 1272893353);
		c = MD5.hh(c, d, a, b, k[7], 16, -155497632);
		b = MD5.hh(b, c, d, a, k[10], 23, -1094730640);
		a = MD5.hh(a, b, c, d, k[13], 4, 681279174);
		d = MD5.hh(d, a, b, c, k[0], 11, -358537222);
		c = MD5.hh(c, d, a, b, k[3], 16, -722521979);
		b = MD5.hh(b, c, d, a, k[6], 23, 76029189);
		a = MD5.hh(a, b, c, d, k[9], 4, -640364487);
		d = MD5.hh(d, a, b, c, k[12], 11, -421815835);
		c = MD5.hh(c, d, a, b, k[15], 16, 530742520);
		b = MD5.hh(b, c, d, a, k[2], 23, -995338651);

		a = MD5.ii(a, b, c, d, k[0], 6, -198630844);
		d = MD5.ii(d, a, b, c, k[7], 10, 1126891415);
		c = MD5.ii(c, d, a, b, k[14], 15, -1416354905);
		b = MD5.ii(b, c, d, a, k[5], 21, -57434055);
		a = MD5.ii(a, b, c, d, k[12], 6, 1700485571);
		d = MD5.ii(d, a, b, c, k[3], 10, -1894986606);
		c = MD5.ii(c, d, a, b, k[10], 15, -1051523);
		b = MD5.ii(b, c, d, a, k[1], 21, -2054922799);
		a = MD5.ii(a, b, c, d, k[8], 6, 1873313359);
		d = MD5.ii(d, a, b, c, k[15], 10, -30611744);
		c = MD5.ii(c, d, a, b, k[6], 15, -1560198380);
		b = MD5.ii(b, c, d, a, k[13], 21, 1309151649);
		a = MD5.ii(a, b, c, d, k[4], 6, -145523070);
		d = MD5.ii(d, a, b, c, k[11], 10, -1120210379);
		c = MD5.ii(c, d, a, b, k[2], 15, 718787259);
		b = MD5.ii(b, c, d, a, k[9], 21, -343485551);

		x[0] = MD5.add32(a, x[0]);
		x[1] = MD5.add32(b, x[1]);
		x[2] = MD5.add32(c, x[2]);
		x[3] = MD5.add32(d, x[3]);
	};

	MD5.cmn = function (q, a, b, x, s, t) {
		a = MD5.add32(MD5.add32(a, q), MD5.add32(x, t));
		return MD5.add32((a << s) | (a >>> (32 - s)), b);
	};

	MD5.ff = function (a, b, c, d, x, s, t) {
		return MD5.cmn((b & c) | ((~b) & d), a, b, x, s, t);
	};

	MD5.gg = function (a, b, c, d, x, s, t) {
		return MD5.cmn((b & d) | (c & (~d)), a, b, x, s, t);
	};

	MD5.hh = function (a, b, c, d, x, s, t) {
		return MD5.cmn(b ^ c ^ d, a, b, x, s, t);
	};

	MD5.ii = function (a, b, c, d, x, s, t) {
		return MD5.cmn(c ^ (b | (~d)), a, b, x, s, t);
	};

	MD5.md51 = function (s) {
		var n = s.length,
			state = [1732584193, -271733879, -1732584194, 271733878], i;
		for (i = 64; i <= s.length; i += 64) {
			MD5.md5cycle(state, MD5.md5blk(s.substring(i - 64, i)));
		}
		s = s.substring(i - 64);
		var tail = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		for (i = 0; i < s.length; i++) {
			tail[i >> 2] |= s.charCodeAt(i) << ((i % 4) << 3);
		}
		tail[i >> 2] |= 0x80 << ((i % 4) << 3);
		if (i > 55) {
			MD5.md5cycle(state, tail);
			for (i = 0; i < 16; i++) {
				tail[i] = 0;
			}
		}
		tail[14] = n * 8;
		MD5.md5cycle(state, tail);
		return state;
	};

	MD5.md5blk = function (s) {
		var md5blks = [], i;
		for (i = 0; i < 64; i += 4) {
			md5blks[i >> 2] = s.charCodeAt(i)
				+ (s.charCodeAt(i + 1) << 8)
				+ (s.charCodeAt(i + 2) << 16)
				+ (s.charCodeAt(i + 3) << 24);
		}
		return md5blks;
	};

	MD5.hex_chr = '0123456789abcdef'.split('');

	MD5.rhex = function (n) {
		var s = '', j = 0;
		for (; j < 4; j++) {
			s += MD5.hex_chr[(n >> (j * 8 + 4)) & 0x0F]
				+ MD5.hex_chr[(n >> (j * 8)) & 0x0F];
		}
		return s;
	};

	MD5.hex = function (x) {
		for (var i = 0; i < x.length; i++) {
			x[i] = MD5.rhex(x[i]);
		}
		return x.join('');
	};

	MD5.md5 = function (s) {
		if (s instanceof Uint8Array) {
			var buffer = "";
			for (var index = 0; index < s.byteLength; index++) {
				buffer += String.fromCharCode(s[index]);
			}
			s = buffer;
		}
		return MD5.hex(MD5.md51(s));
	};

	MD5.add32 = function (a, b) {
		return (a + b) & 0xFFFFFFFF;
	};

	if (MD5.md5('hello') !== '5d41402abc4b2a76b9719d911017c592') {
		MD5.add32 = function (x, y) {
			var lsw = (x & 0xFFFF) + (y & 0xFFFF),
				msw = (x >> 16) + (y >> 16) + (lsw >> 16);
			return (msw << 16) | (lsw & 0xFFFF);
		};
	}
}

/** @namespace */
var FileAPI = FileAPI || {};

/** FileAPI static */
{
	FileAPI.directorySeparator = "/";

	FileAPI.isValidFileName = function (name) {
		return name !== undefined
			&& name.length > 0
			&& name.match(/[/\\\0]/g) === null
			&& !(name.indexOf(".") === 0 && name.length === 1)
			&& !(name.indexOf("..") === 0 && name.length === 2);
	};

	FileAPI.isValidPathName = function (name) {
		return name !== undefined && name.match(/[\0]/g) === null;
	};

	FileAPI.splitFile = function (fullPath) {
		if (!this.isValidPathName(fullPath)) {
			throw new Error(fullPath + " is not a valid path");
		}
		return fullPath.trim().match(/(.*\/)?([^/]+)/).splice(1);
	};

	FileAPI.splitPath = function (path) {
		if (!this.isValidPathName(path)) {
			throw new Error(path + " is not a valid path");
		}
		return path.replace(/\/$/g, "").split(/[\/\\]/);
	};

	FileAPI.cast = function (object, prototype) {
		return Object.setPrototypeOf(object, prototype);
	}
}

/** FileAPI.Platform */
{
	/**
	 * Platform for every implementation from the API
	 *
	 * @interface
	 */
	FileAPI.Platform = function () {
		var self = this;
		/**
		 * Events triggered / registered
		 *
		 * createFile, when new file is created
		 * updateFile, when file is updated
		 * removeFile, when file is removed
		 * createDirectory, when directory is created
		 * updateDirectory, when directory content is updated
		 * removeDirectory, when directory is removed
		 */
		self.events = function () {
			var eventSelf = this;

			eventSelf.listeners = {};

			return {
				// trigger event
				fire : function (eventName, eventArgs) {
					if ((typeof eventName) === "undefined") {
						return;
					}
					var queue = eventSelf.listeners[eventName];
					if ((typeof queue) === "undefined") {
						return;
					}
					var priorities = Object.keys(queue).sort();
					for (var i = priorities.length - 1; i >= 0; i--) {
						var callback = queue[priorities[i]];
						try {
							if ((typeof callback) === "function") {
								callback(eventArgs);
							}
						} catch (ex) {
							console.error(ex);
						}
					}
				},
				// register event
				on : function (eventName, callback, prio) {
					if ((typeof callback) !== "function") {
						throw new Error("Callback needs to be a function, given: " + (typeof callback));
					}
					var queue = eventSelf.listeners[eventName];
					if ((typeof queue) === "undefined") {
						queue = eventSelf.listeners[eventName] = {};
					}
					if ((typeof prio) !== "number") {
						prio = 1;
					}
					while (prio in queue) {
						prio -= 0.000001;
					}
					queue[prio] = callback;
				},
			};
		}();

		self.loopTrough = function (path, fn, open, recursive) {
			if ((typeof path) === "string") {
				path = self.open(path);
			}
			if (path === undefined || path === null) {
				throw new Error("FileNotFoundException");
			}
			if (path.isFile()) {
				fn((open ? path : path.fullPath), "file");
				return;
			}
			recursive = recursive !== undefined && recursive;
			var dir = path;
			path = path.fullPath;
			fn((open ? dir : path), "directory");
			dir.children.forEach(function (childPath) {
				childPath = path + childPath;
				var child = open ? self.open(childPath) : null;
				if ((open ? child.isDirectory() : self.hasDirectory(childPath))) {
					if (recursive) {
						self.loopTrough(childPath, fn, open, recursive);
					} else {
						fn((open ? dir : childPath), "directory");
					}
				} else if ((open ? child.isFile() : self.hasFile(childPath))) {
					fn((open ? child : childPath), "file");
				} else {
					console.error("LOOP ERROR ==>> " + childPath + "\t@ " + path)
				}
			});
		};
	};

	/**
	 * @returns {boolean} true if platform is supported
	 */
	FileAPI.Platform.prototype.isSupported = function () { return false; };
	/**
	 * @param {FileAPI.Directory|string} path the path to check
	 * @returns {boolean} true if there is a directory
	 */
	FileAPI.Platform.prototype.hasDirectory = function (path) { return false; };
	/**
	 * @param {FileAPI.File|string} path the path to check
	 * @returns {boolean} true if there is a file
	 */
	FileAPI.Platform.prototype.hasFile = function (path) { return false; };
	/**
	 * @param {FileAPI.Entry|string} path Path of the file or directory to check
	 * @returns {boolean} true if there is a file or directory
	 */
	FileAPI.Platform.prototype.exists = function (path) { return hasFile(path) || hasDirectory(path); };
	/**
	 * @param {(FileAPI.Directory|string)} path the path to the directory should be created in
	 * @param {(FileAPI.Entry|FileAPI.Entry[]|string|string[])} children files or direcotries to add
	 * @returns {boolean} true if succeeded
	 */
	FileAPI.Platform.prototype.createDirectory = function (path, children) { return false; };
	/**
	 * @param {(FileAPI.Directory|string)} directory to remove
	 * @param {boolean} recursive true if files inside directory should be removed as well
	 * @returns {boolean} true if succeeded
	 * @throws {FileNotFoundException} When directory is not found
	 * @throws {IOException} When directory is not empty and recursive is false
	 */
	FileAPI.Platform.prototype.removeDirectory = function (directory, recursive) { throw new Error("Not implemented yet"); };
	/**
	 * @param {(FileAPI.File|Uint8Array|string)} data to save
	 * @param {(string)} [path] the path where the file should be created in
	 * @returns {boolean} true if succeeded
	 * @throws {NoPathException} When there is no path defined to create the file
	 */
	FileAPI.Platform.prototype.createFile = function (data, path) { return false; };
	/**
	 * @param {(FileAPI.Directory|string)} path the directory where you want to list your files from
	 * @returns {FileAPI.Directory} list of files and directories
	 * @throws {FileNotFoundException} When directory is not found
	 */
	FileAPI.Platform.prototype.openDirectory = function (path) { return []; };
	/**
	 * @param {(FileAPI.File|string)} path the path the file will be read from
	 * @returns {FileAPI.File} File requested
	 * @throws {FileNotFoundException} When file is not found
	 */
	FileAPI.Platform.prototype.openFile = function (path) { throw new Error("Not implemented yet"); };
	/**
	 * @param {(FileAPI.File)} file file to save
	 * @returns {boolean} true if succeeded
	 * @throws {IOException} When something went wrong saving
	 */
	FileAPI.Platform.prototype.saveFile = function (file) { throw new Error("IOException: Not implemented yet"); };
	/**
	 * @param {(FileAPI.File|string)} file the original file
	 * @param {(string)} path the path where to store the copy
	 * @returns {boolean} true if succeeded
	 * @throws {FileNotFoundException} When file is not found
	 * @throws {IOException} When something went wrong saving
	 */
	FileAPI.Platform.prototype.copyFile = function (file, path) { throw new Error("IOException: Not implemented yet"); };
	/**
	 * @param {(FileAPI.File|string)} path the file to remove
	 * @returns {boolean} true if succeeded
	 * @throws {FileNotFoundException} When file is not found
	 */
	FileAPI.Platform.prototype.removeFile = function (path) { throw new Error("Not implemented yet"); };
	/**
	 * @param {(string)} path the file or directory to open
	 * @returns {FileAPI.Entry} file or directory requested
	 * @throws {FileNotFoundException} When file or directory is not found
	 */
	FileAPI.Platform.prototype.open = function (path) {
		throw new Error("Not implemented yet");
	};
	/**
	 * @callback loopTroughCallback
	 * @param {FileAPI.Entry} entry inside directory
	 */

	/**
	 * @param {(FileAPI.Entry|string)} path the file or directory to open
	 * @param {(loopTroughCallback|function)} fn callback that gets called for each file and directory. <br> first param the type, second param the
	 * @param {(boolean)} [open] should it open the file, or only list the existence
	 * @param {(boolean)} [recursive] indicates if it should loop recursive inside other directories
	 * @throws FileNotFoundException when no valid location
	 */
	FileAPI.Platform.prototype.loopTrough = function (path, fn, open, recursive) { return false; };
}
/** FileAPI.Entry */
{
	/**
	 * Interface File OR Directory
	 * Superclass to both of them
	 *
	 * @interface
	 */
	FileAPI.Entry = function () {
	};
	/**
	 * @returns {boolean} true if it's a directory
	 */
	FileAPI.Entry.prototype.isDirectory = function () { return false; };
	/**
	 * @returns {boolean} true if it's a file
	 */
	FileAPI.Entry.prototype.isFile = function () { return false; };
	/**
	 * @param {FileAPI.Platform} platform platform to save to
	 * @returns {FileAPI.Entry} returns self
	 */
	FileAPI.Entry.prototype.save = function (platform) { return this; };
	/**
	 * @param {FileAPI.Platform} platformFrom platform to move from
	 * @param {FileAPI.Platform} platformTo platform to copy to
	 * @param {string} pathTo the path the copy will be copied to
	 * @returns {FileAPI.Entry} clone on new location
	 */
	FileAPI.Entry.prototype.copy = function (platformFrom, platformTo, pathTo) { return this; };
	/**
	 * @param {FileAPI.Platform} platformFrom platform to move from
	 * @param {FileAPI.Platform} platformTo platform to move to
	 * @param {String} pathTo path to write to
	 * @returns {FileAPI.Entry} this on the new location
	 */
	FileAPI.Entry.prototype.move = function (platformFrom, platformTo, pathTo) { return this; };
	/**
	 * @param {FileAPI.Platform} platform platform to remove to
	 * @returns {boolean} true if succeeded
	 */
	FileAPI.Entry.prototype.remove = function (platform) { return false; };
	/**
	 * @returns {string} this object, but then as a string
	 */
	FileAPI.Entry.prototype.deserialize = function () { return ""; };
	/**
	 * @param {string} serialized object represented as a string
	 * @returns {FileAPI.Entry} this object, but then as a string
	 */
	FileAPI.Entry.serialize = function (serialized) { return null; };
}
/** FileAPI.File */
{
	/**
	 * @class
	 * @param {string} path the path its located in
	 * @param {Uint8Array|string} data the data that its stored
	 * @param {Object} [options] metadata
	 * @implements {FileAPI.Entry}
	 */
	FileAPI.File = function (path, data, options) {
		FileAPI.Entry.call(this);

		if (!path.startsWith(FileAPI.directorySeparator)) {
			path = FileAPI.directorySeparator + path;
		}
		var split = FileAPI.splitPath(path);
		if (split.length === 0) {
			this.name = "";
			this.path = "";
		} else if (split.length === 1) {
			this.name = split[0];
			this.path = FileAPI.directorySeparator;
		} else {
			this.name = split[split.length - 1];
			this.path = split.slice(0, split.length - 1).join(FileAPI.directorySeparator) + FileAPI.directorySeparator;
		}
		if (this.name.length === 0) {
			throw new Error("Try to create a no named file");
		}
		this.fullPath = path;
		if (options !== null && (typeof options) === "object") {
			this.options = options;
			if ((typeof options.type) === "string") {
				this.type = options.type;
			}
			if ((typeof options.lastModified) === "number") {
				this.lastModified = options.lastModified;
			}
		} else {
			this.options = {};
		}
		if (this.type === undefined) {
			if (data instanceof Uint8Array) {
				this.options.type = this.type = "uint8array";
			} else if ((typeof data) === "string") {
				this.options.type = this.type = "string";
			}
		}
		if (this.lastModified === undefined) {
			this.options.lastModified = this.lastModified = Date.now();
		}
		this.data = data;
		this.hash = MD5.md5(data);
	};
	/**
	 * @inheritdoc
	 * @override
	 */
	FileAPI.File.prototype.isDirectory = function () { return false; };
	/**
	 * @inheritdoc
	 * @override
	 */
	FileAPI.File.prototype.isFile = function () { return true; };
	/**
	 * @inheritdoc
	 * @override
	 */
	FileAPI.File.prototype.save = function (platform) { return platform.saveFile(this); };
	/**
	 * @inheritdoc
	 * @override
	 */
	FileAPI.File.prototype.copy = function (platformFrom, platformTo, pathTo) {
		var clone = new FileAPI.File(pathTo, this.data, this.options);
		platformTo.createFile(clone);
		return clone;
	};
	/**
	 * @inheritdoc
	 * @override
	 */
	FileAPI.File.prototype.move = function (platformFrom, platformTo, pathTo) {
		var clone = new FileAPI.File(pathTo, this.data, this.options);
		platformTo.createFile(clone);
		platformFrom.removeFile(this);
		return clone;
	};
	/**
	 * @inheritdoc
	 * @override
	 */
	FileAPI.File.prototype.remove = function (platform) {
		return platform.removeFile(this);
	};
	/**
	 * @inheritdoc
	 * @override
	 */
	FileAPI.File.prototype.deserialize = function () {
		if (this.type === "string") {
			return JSON.stringify({ fullPath : this.fullPath, data : this.data, options : this.options });
		} else if (this.type === "uint8array") {
			return JSON.stringify({
									  fullPath : this.fullPath,
									  uint8array : [].slice.call(this.data),
									  options : this.options,
								  });
		}
		var base64 = btoa(String.fromCharCode(this.data));
		return JSON.stringify({ fullPath : this.fullPath, data64 : this.base64, options : this.options });
	};
	/**
	 * @inheritdoc
	 * @override
	 */
	FileAPI.File.serialize = function (serialized) {
		var json = JSON.parse(serialized);
		var data;
		if (json.data64 !== undefined) {
			var base64 = atob(json.data64);
			var buff = [];
			for (var i = 0; i < base64.length; i++) {
				buff[i] = base64[i].charCodeAt(0);
			}
			data = new Uint8Array(buff);
		}
		if (json.uint8array !== undefined) {
			data = new Uint8Array(json.uint8array);
		} else {
			data = json.data;
		}
		return new FileAPI.File(json.fullPath, data, json.options);
	};
}
/** FileAPI.Directory */
{
	/**
	 * @class
	 * @param {string} path path this directory is located in
	 * @param {array} [children] all entries inside directory
	 * @implements {FileAPI.Entry}
	 */
	FileAPI.Directory = function (path, children) {
		FileAPI.Entry.call(this);

		if (!path.startsWith(FileAPI.directorySeparator)) {
			path = FileAPI.directorySeparator + path;
		}
		var split = FileAPI.splitPath(path);
		this.fullPath = path;
		this.name = split[split.length - 1] + FileAPI.directorySeparator;
		this.path = split.slice(0, split.length - 1).join(FileAPI.directorySeparator) + FileAPI.directorySeparator;
		if (children === undefined) {
			children = [];
		}
		this.children = children;
	};
	/**
	 * @inheritdoc
	 * @override
	 */
	FileAPI.Directory.prototype.isDirectory = function () { return true; };
	/**
	 * @inheritdoc
	 * @override
	 */
	FileAPI.Directory.prototype.isFile = function () { return false; };
	/**
	 * @inheritdoc
	 * @override
	 */
	FileAPI.Directory.prototype.save = function (platform) { platform.createDirectory(this); };
	/**
	 * @inheritdoc
	 * @override
	 */
	FileAPI.Directory.prototype.copy = function (platformFrom, platformTo, pathTo) {
		platformTo.createDirectory(pathTo);
		for (var i = 0; i < children.length; i++) {
			var childName = children[i];
			var childPath = fullPath + FileAPI.directorySeparator + childName;
			var child = platformFrom.open(childPath);
			child.copy(platformFrom, platformTo, pathTo + FileAPI.directorySeparator + childName);
		}
		return new FileAPI.Directory(this.children, pathTo);
	};
	/**
	 * @inheritdoc
	 * @override
	 */
	FileAPI.Directory.prototype.move = function (platformFrom, platformTo, pathTo) {
		platformTo.createDirectory(pathTo);
		for (var i = 0; i < children.length; i++) {
			var childName = children[i];
			var childPath = fullPath + FileAPI.directorySeparator + childName;
			var child = platformFrom.open(childPath);
			child.copy(platformFrom, platformTo, pathTo + FileAPI.directorySeparator + childName);
		}
		platformFrom.removeDirectory(this.fullPath, true);
		return new FileAPI.Directory(this.children, pathTo);
	};
	/**
	 * @inheritdoc
	 * @override
	 */
	FileAPI.Directory.prototype.remove = function (platform) {
		return platform.removeDirectory(this);
	};
	/**
	 * @inheritdoc
	 * @override
	 */
	FileAPI.Directory.prototype.deserialize = function () {
		return JSON.stringify({ fullPath : this.fullPath, children : this.children });
	};
	/**
	 * @inheritdoc
	 * @override
	 */
	FileAPI.Directory.serialize = function (serialized) {
		var json = JSON.parse(serialized);
		return new FileAPI.Directory(json.fullPath, json.children);
	};
	/**
	 * @returns {boolean} true when its root
	 */
	FileAPI.Directory.prototype.isRoot = function () {
		return this.path.length === 0 || this.path === FileAPI.directorySeparator;
	};
}

// Implementations of Platform
// Key-Value Platforms
{
	/**
	 * @interface
	 * @param {string} drive a key to identify drive
	 * @implements {FileAPI.Platform}
	 */
	FileAPI.KVPlatform = function (drive) {
		FileAPI.Platform.call(this);

		var self = this;
		var directoryIdentifier = "D~";
		var fileIdentifier = "F~";
		if (drive === undefined) {
			drive = "";
		}
		var drive = drive;
		// This method should be implemented BEFORE creating instance of KVPlatform
		if ((typeof this.getKVSetMethod) !== "function") {
			this.isSupported = function () {
				return false;
			};
			return;
		} else {
			this.isSupported = function () {
				return true;
			};
		}
		// function set(key, value):void
		this.setValue = this.getKVSetMethod();
		// function get(key):value	[null if non]
		this.getValue = this.getKVGetMethod();
		// function has(key):bool	true if exists
		this.hasValue = this.getKVHasMethod();
		// function remove(key):void
		this.removeValue = this.getKVRemoveMethod();


		// Public Methods

		this.hasDirectory = function (path) {
			if ((typeof path) === "undefined") {
				throw new Error("No Path given");
			}
			if ((typeof  path) !== "string") {
				path = path.fullPath;
			}
			path = self._addDrive(path);
			return this.hasValue(path) && this.getValue(path).startsWith(directoryIdentifier);
		};

		this.hasFile = function (path) {
			if ((typeof path) === "undefined") {
				throw new Error("No Path given");
			}
			if ((typeof  path) !== "string") {
				path = path.fullPath;
			}
			path = self._addDrive(path);
			return this.hasValue(path) && this.getValue(path).startsWith(fileIdentifier);
		};

		this.open = function (path) {
			if (self.hasFile(path)) {
				return self.openFile(path);
			}
			if (self.hasDirectory(path)) {
				return self._readDirectory(path);
			}
			throw new Error("FileNotFoundException: " + path);
		};

		this.createDirectory = function (path, children) {
			if ((typeof path) === "undefined") {
				throw new Error("No Path given");
			}
			if (children === undefined) {
				children = [];
			} else {
				if (children.__proto__ === Array.prototype) {
					if (children.length === 0) {
						children = [];
					} else {
						children.forEach(function (child) {
							// TODO: handle children: FileAPI.Entry|FileAPI.Entry[]|string|string[]
						});
					}
				}
			}
			if (path instanceof FileAPI.Directory) {
				path = path.fullPath;
			}
			path = path.trim().replace(/[\/\\]/g, FileAPI.directorySeparator).replace(/\/+$/g, FileAPI.directorySeparator).replace(/^\/+/, "");
			var existingDirectory = self._readDirectory(path);
			if (existingDirectory !== null) {
				return false;
			}
			var route = FileAPI.splitPath(path);
			var trace = "";
			route.forEach(function (step) {
				var childParent = self._readDirectory(trace);
				if (childParent !== null) {
					childParent.children.pushDistinct(step + FileAPI.directorySeparator);
					self._writeDirectoryObject(childParent);
				} else {
					console.error("something, something error msg here -> " + route + " :: " + path);
				}
				trace += step + FileAPI.directorySeparator;
				if (self.hasDirectory(trace)) {
					return;
				}
				self._writeDirectoryObject(new FileAPI.Directory(trace, children));
			});
			return true;
		};

		this.openDirectory = function (path) {
			if ((typeof path) === "undefined") {
				throw new Error("No Path given");
			}
			if ((typeof path) === "string") {
				path = self._readDirectory(path);
			}
			if (path === undefined || path === null) {
				throw new Error("FileNotFoundException");
			}
			return path;
		};

		this.removeDirectory = function (directory, recursive) {
			if ((typeof directory) === "undefined") {
				throw new Error("No directory given");
			}
			if ((typeof directory) === "string") {
				directory = self._readDirectory(directory);
			}
			if (directory === undefined || directory === null) {
				throw new Error("FileNotFoundException");
			}
			if (!recursive && directory.children.length > 0) {
				throw new Error("IOException: Directory not empty");
			}
			if (directory.children.length > 0) {
				var children = directory.children;
				for (var i = 0; i < children.length; i++) {
					var pathChild = directory.fullPath + children[i];
					if (self.hasDirectory(pathChild)) {
						self.removeDirectory(pathChild, recursive);
					} else {
						self.removeFile(pathChild);
					}
				}
			}
			var parentDir = self._readDirectory(directory.path);
			if (parentDir === null) {
				throw new Error("Tried to remove file to non-existing directory " + file.path);
			}
			parentDir.children.removeItem(directory.name);
			this._writeDirectoryObject(parentDir);

			this.events.fire("removeDirectory", directory.fullPath);
			self._remove(directory.fullPath);
			return true;
		};

		this.createFile = function (data, path) {
			if (data instanceof FileAPI.File) {
				self._writeFileObject(data);
				return true;
			}
			if (path === undefined || (typeof path) !== "string") {
				throw new Error("NoPathException");
			}
			self._writeFileObject(new FileAPI.File(path, data));
			return true;
		};

		this.copyFile = function (file, path) {
			if ((typeof file) === "string") {
				file = this.openFile(file);
			}
			if (file === undefined || file === null) {
				throw new Error("FileNotFoundException");
			}
			file.copy(this, this, path);
			return true;
		};

		this.openFile = function (path) {
			if ((typeof path) === "string") {
				path = self._readFile(path);
			}
			if (path === undefined || path === null) {
				throw new Error("FileNotFoundException");
			}
			return path;
		};

		this.saveFile = function (file) {
			if (file instanceof FileAPI.File) {
				self._writeFileObject(file);
				return true;
			}
			throw new Error("IOException: No file given");
		};

		this.removeFile = function (file) {
			if ((typeof file) === "string") {
				file = self._readFile(file);
			}
			if (file === undefined || file === null) {
				throw new Error("FileNotFoundException");
			}
			var parentDir = self._readDirectory(file.path);
			if (parentDir === null) {
				throw new Error("Tried to remove file to non-existing directory " + file.path);
			}
			parentDir.children.removeItem(file.name);
			this._writeDirectoryObject(parentDir);

			this.events.fire("removeFile", file.fullPath);
			this._remove(file.fullPath);
			return true;
		};

		// Private Methods

		this._addDrive = function (path) {
			return drive + FileAPI.directorySeparator + path.replace(/^\//g, "");
		};

		this._readFile = function (fullPath) {
			var serialized = self.getValue(self._addDrive(fullPath));
			if (serialized === null) {
				return null;
			}
			if (serialized.indexOf(fileIdentifier) !== 0) {
				return null;
			}
			serialized = serialized.substr(fileIdentifier.length);
			return FileAPI.File.serialize(serialized);
		};

		this._writeFileObject = function (file) {
			return this._writeFile(file.path, file.name, file.deserialize());
		};

		this._writeFile = function (directory, filename, fileString) {
			var fullPath = directory + filename;
			var parentDir = self._readDirectory(directory);
			if (parentDir === null) {
				throw new Error("Tried to add file to non-existing directory " + parent);
			}
			parentDir.children.pushDistinct(filename);
			this._writeDirectoryObject(parentDir);
			fullPath = self._addDrive(fullPath);
			var isNew = !this.hasValue(fullPath);
			this.setValue(fullPath, fileIdentifier + fileString);
			if (isNew) {
				this.events.fire("createFile", fullPath);
			} else {
				this.events.fire("updateFile", fullPath);
			}
		};

		this._readDirectory = function (fullPath) {
			if (!fullPath.endsWith(FileAPI.directorySeparator)) {
				fullPath += FileAPI.directorySeparator;
			}
			var serialized = self.getValue(self._addDrive(fullPath));
			if (serialized === null) {
				return null;
			}
			if (serialized.indexOf(directoryIdentifier) !== 0) {
				return null;
			}
			serialized = serialized.substr(directoryIdentifier.length);
			return FileAPI.Directory.serialize(serialized);
		};

		this._writeDirectoryObject = function (directory) {
			self._writeDirectory(directory.fullPath, directory.deserialize());
		};

		this._writeDirectory = function (fullPath, directoryString) {
			var isNew = !this.hasValue(fullPath);
			self.setValue(self._addDrive(fullPath), directoryIdentifier + directoryString);
			if (isNew) {
				this.events.fire("createDirectory", fullPath);
			} else {
				this.events.fire("updateDirectory", fullPath);
			}
		};

		this._remove = function (fullPath) {
			if (fullPath === drive) {
				this._createRoot();
			} else {
				self.removeValue(self._addDrive(fullPath));
			}
		};

		this._createRoot = function () {
			self._writeDirectoryObject(new FileAPI.Directory("/"));
		};

		if (!this.hasDirectory("")) {
			this._createRoot();
		}
	};

	/**
	 * @class
	 * @param {string} drive a key to identify drive
	 * @implements {FileAPI.KVPlatform}
	 */
	FileAPI.SessionPlatform = function (drive) {
		// function set(key, value):void
		this.getKVSetMethod = function () {
			return function (key, value) {
				sessionStorage.setItem(key, value);
			};
		};
		// function get(key):value	[null if non]
		this.getKVGetMethod = function () {
			return function (key) {
				return sessionStorage.getItem(key);
			};
		};
		// function has(key):bool	true if exists
		this.getKVHasMethod = function () {
			return function (key) {
				return sessionStorage.getItem(key) !== null;
			};
		};
		// function remove(key):void
		this.getKVRemoveMethod = function () {
			return function (key) {
				sessionStorage.removeItem(key);
			};
		};

		FileAPI.KVPlatform.call(this, drive);
	};

	/**
	 * @class
	 * @param {string} drive a key to identify drive
	 * @implements {FileAPI.KVPlatform}
	 */
	FileAPI.LocalStoragePlatform = function (drive) {
		// function set(key, value):void
		this.getKVSetMethod = function () {
			return function (key, value) {
				localStorage.setItem(key, value);
			};
		};
		// function get(key):value	[null if non]
		this.getKVGetMethod = function () {
			return function (key) {
				return localStorage.getItem(key);
			};
		};
		// function has(key):bool	true if exists
		this.getKVHasMethod = function () {
			return function (key) {
				return localStorage.getItem(key) !== null;
			};
		};
		// function remove(key):void
		this.getKVRemoveMethod = function () {
			return function (key) {
				localStorage.removeItem(key);
			};
		};

		FileAPI.KVPlatform.call(this, drive);
	};

	/**
	 * @class
	 * @implements {FileAPI.KVPlatform}
	 */
	FileAPI.MemoryPlatform = function () {
		var self = this;
		self.memory = {};

		// function set(key, value):void
		this.getKVSetMethod = function () {
			return function (key, value) {
				self.memory[key] = value;
			};
		};
		// function get(key):value	[null if non]
		this.getKVGetMethod = function () {
			return function (key) {
				var found = self.memory[key];
				if (found === undefined) {
					return null;
				}
				return found;
			};
		};
		// function has(key):bool	true if exists
		this.getKVHasMethod = function () {
			return function (key) {
				return self.memory.hasOwnProperty(key);
			};
		};
		// function remove(key):void
		this.getKVRemoveMethod = function () {
			return function (key) {
				delete self.memory[key];
			};
		};

		FileAPI.KVPlatform.call(this, "");
	};
}