var FileAPI = FileAPI || {};

FileAPI.LogChanges = function (platform, HTMLElement) {
	platform.events.on("createFile", function (args) {
		HTMLElement.innerHTML += new Date().toLocaleTimeString() + " Create File: " + args + "<br>";
	});
	platform.events.on("updateFile", function (args) {
		HTMLElement.innerHTML += new Date().toLocaleTimeString() + " Update File: " + args + "<br>";
	});
	platform.events.on("removeFile", function (args) {
		HTMLElement.innerHTML += new Date().toLocaleTimeString() + " Removed File: " + args + "<br>";
	});
	platform.events.on("createDirectory", function (args) {
		HTMLElement.innerHTML += new Date().toLocaleTimeString() + " Create Dir: " + args + "<br>";
	});
	platform.events.on("updateDirectory", function (args) {
		HTMLElement.innerHTML += new Date().toLocaleTimeString() + " Update contents: " + args + "<br>";
	});
	platform.events.on("removeDirectory", function (args) {
		HTMLElement.innerHTML += new Date().toLocaleTimeString() + " Removed Dir: " + args + "<br>";
	});
};

FileAPI.ShowDirectory = function (platform, HTMLElement) {
	var rng = Math.round(Math.random() * 10000); // create unique id for files

	var create = function (key, fullPath) {
		var parentDiv = HTMLElement;
		var split = FileAPI.splitPath(fullPath);
		var name;
		var path;
		if (split.length === 0) {
			name = "";
			path = "";
		} else if (split.length === 1) {
			name = split[0];
			path = FileAPI.directorySeparator;
		} else {
			name = split[split.length - 1];
			path = split.slice(0, split.length - 1).join(FileAPI.directorySeparator) + FileAPI.directorySeparator;
		}
		var idPath = "d" + rng + "-" + path;
		var existingPath = document.getElementById(idPath);
		if (existingPath !== null) {
			parentDiv = existingPath;
		}

		var id = key.substr(0, 1) + rng + "-" + fullPath;
		var existing = document.getElementById(id);
		if (existing !== null) {
			return;
		}
		var newDiv = document.createElement("div");
		var a = document.createElement("a");
		newDiv.id = id;
		newDiv.classList.add("FileAPI-" + key);
		a.innerText = fullPath;
		if ((typeof FileAPI.export) !== "undefined") {
			var dl = document.createElement("a");
			if (key === "file") {
				dl.innerText = "[V]";
				dl.href = "#" + fullPath;
			} else {
				dl.innerText = "[>]";
			}
			a.innerText = "\t \t" + a.innerText;
			dl.onclick = function () {
				if (key === "file") {
					FileAPI.export(platform, fullPath);
				}
				return false;
			};
			newDiv.append(dl);
		}
		newDiv.append(a);
		parentDiv.append(newDiv);
	};

	platform.events.on("createFile", function (args) {
		create("file", args);
	});
	platform.events.on("createDirectory", function (args) {
		create("directory", args);
	});

	platform.events.on("removeFile", function (args) {
		var id = "f" + rng + "-" + args;
		var existing = document.getElementById(id);
		if (existing !== null) {
			existing.remove();
		}
	});

	platform.events.on("removeDirectory", function (args) {
		var id = "d" + rng + "-" + args;
		var existing = document.getElementById(id);
		if (existing !== null) {
			existing.remove();
		}
	});

	platform.loopTrough("/", function (fullPath, type) {
		create(type, fullPath);
	}, false, true);
};

FileAPI.Controls = function (platform, HTMLElement) {
	var br = function () {return document.createElement("br")};

	// Create Directory
	{
		var createDirectoryLabel = document.createElement("span");
		var createDirectoryInput = document.createElement("input");
		var createDirectoryButton = document.createElement("button");
		createDirectoryLabel.innerText = "Directory Name: ";
		createDirectoryButton.textContent = "CreateDirectory";
		createDirectoryButton.onclick = function () {
			platform.createDirectory(createDirectoryInput.value);
			createDirectoryInput.value = "";
		};
		HTMLElement.append(createDirectoryLabel);
		HTMLElement.append(br());
		HTMLElement.append(createDirectoryInput);
		HTMLElement.append(createDirectoryButton);
		HTMLElement.append(br());
		HTMLElement.append(br());
	}
	// Create file
	{
		var createFileLabel = document.createElement("span");
		var createFileInput = document.createElement("input");
		var openFileButton = document.createElement("button");
		var createFileTextarea = document.createElement("textarea");
		var createFileButton = document.createElement("button");
		createFileLabel.innerText = "File Name: ";
		openFileButton.textContent = "OpenFile";
		createFileTextarea.rows = 4;
		createFileTextarea.cols = 50;
		createFileButton.textContent = "SaveFile";
		openFileButton.onclick = function () {
			var file = platform.openFile(createFileInput.value);
			createFileTextarea.value = file.data;
		};
		createFileButton.onclick = function () {
			platform.createFile(createFileTextarea.value, createFileInput.value);
			createFileInput.value = "";
			createFileTextarea.value = "";
		};
		HTMLElement.append(createFileLabel);
		HTMLElement.append(createFileInput);
		HTMLElement.append(openFileButton);
		HTMLElement.append(br());
		HTMLElement.append(createFileTextarea);
		HTMLElement.append(br());
		HTMLElement.append(createFileButton);
		HTMLElement.append(br());
		HTMLElement.append(br());
	}

	// Remove Directory
	{
		var removeDirectoryLabel = document.createElement("span");
		var removeDirectoryInput = document.createElement("input");
		var removeDirectoryButton = document.createElement("button");
		removeDirectoryLabel.innerText = "Directory Name: ";
		removeDirectoryButton.textContent = "RemoveDirectory";
		removeDirectoryButton.onclick = function () {
			platform.removeDirectory(removeDirectoryInput.value);
			removeDirectoryInput.value = "";
		};
		HTMLElement.append(removeDirectoryLabel);
		HTMLElement.append(br());
		HTMLElement.append(removeDirectoryInput);
		HTMLElement.append(removeDirectoryButton);
		HTMLElement.append(br());
		HTMLElement.append(br());
	}

	// Remove Directory
	{
		var removeFileLabel = document.createElement("span");
		var removeFileInput = document.createElement("input");
		var removeFileButton = document.createElement("button");
		removeFileLabel.innerText = "File Name: ";
		removeFileButton.textContent = "RemoveFile";
		removeFileButton.onclick = function () {
			platform.removeFile(removeFileInput.value);
			removeFileInput.value = "";
		};
		HTMLElement.append(removeFileLabel);
		HTMLElement.append(br());
		HTMLElement.append(removeFileInput);
		HTMLElement.append(removeFileButton);
		HTMLElement.append(br());
		HTMLElement.append(br());
	}

};