// $(function () {
    console.log("debugger injected");
    var project = "FileAPI";

    var saysWho = (function () {
        var ua = navigator.userAgent, tem,
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE ' + (tem[1] || '');
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
            if (tem !== null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) !== null) M.splice(1, 1, tem[1]);
        return M.join(' ');
    })();

    var oldOnerror = window.onerror;
    window.onerror = function (errorMsg, url, lineNumber, columnNumber, errorObject) {
        if (oldOnerror === null || oldOnerror === undefined) {
            oldOnerror = function () {
            };
        }
        var file = url.replace(/https?:\/\//, "");
        file = file.substring(file.indexOf("/") + 1);
        if (file.length === 0) {
            file = url.replace(/https?:\/\//, "");
        }
        if (file.length === 0) {
            file = location.href.replace(/https?:\/\//, "")
        }
        var errorClass;
        var message;
        var stack;
        if (errorObject === null || errorObject === undefined) {
            errorClass = "Error";
            message = "Unknown";
            stack = "No stacktrace";
        } else {
            errorClass = errorObject.constructor.name;
            message = errorObject.message;
            stack = errorObject.stack;
        }

        $.ajax({
            url: "https://MakerTim.nl/Bugs/Log/" + encodeURIComponent(saysWho) + "/" + encodeURIComponent(project) + "/1.0/" + encodeURIComponent(errorClass) + "/" + encodeURIComponent(file) + "/" + Math.max(1, lineNumber),
            type: "GET",
            dataType: "json",
            crossDomain: true,
            success: function () {
                oldOnerror(errorMsg, url, lineNumber, columnNumber, errorObject);
            },
            error: function (err) {
                oldOnerror(errorMsg, url, lineNumber, columnNumber, errorObject);
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader("message", message);
                xhr.setRequestHeader("stacktrace", btoa(stack.replace(/\n/g, "\n\t")));
            }
        });
    };
// });