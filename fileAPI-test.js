function assert(condition, message) {
	if (!condition) {
		message = "Failed at " + (message || "unnamed");
		if (typeof Error !== "undefined") {
			throw new Error(message);
		}
		throw message;
	}
}

function assertNot(condition, message) {
	return assert(!condition, message);
}

function assertTry(condition, message) {
	if ((typeof condition) === "function") {
		try {
			condition();
		} catch (ex) {
			message = "Failed at " + (message || "unnamed");
			if (typeof Error !== "undefined") {
				throw new Error(message);
			}
			throw message;
		}
	}
}

function assertFail(condition, message) {
	if ((typeof condition) === "function") {
		try {
			condition();
		} catch (ex) {
			return;
		}
		message = "Failed at " + (message || "unnamed");
		if (typeof Error !== "undefined") {
			throw new Error(message);
		}
		throw message;
	}
}

// Filename Test
{
	assert(FileAPI.isValidFileName("hello"), "hello as file name");
	assert(FileAPI.isValidFileName("Hello"), "Hello as file name");
	assert(FileAPI.isValidFileName("H3llo"), "H3llo as file name");
	assert(FileAPI.isValidFileName("12345"), "12345 as file name");
	assert(FileAPI.isValidFileName("testing.js"), "testing.js as file name");
	assert(FileAPI.isValidFileName("maggie"), "maggie as file name");
	assert(FileAPI.isValidFileName(".git"), ".git as file name");
	assertNot(FileAPI.isValidFileName("."), ". not allowed as file name");
	assert(FileAPI.isValidFileName(".a"), ".a as file name");
	assert(FileAPI.isValidFileName("a."), "a. as file name");
	assertNot(FileAPI.isValidFileName(".."), ".. not allowed as file name");
	assert(FileAPI.isValidFileName("..a"), "..a as file name");
	assert(FileAPI.isValidFileName("a."), "a.. as file name");
	assertNot(FileAPI.isValidFileName("/hello"), "/hello not allowed as file name");
	assertNot(FileAPI.isValidFileName("\\hello"), "\\hello not allowed as file name");
	assertNot(FileAPI.isValidFileName("\0hello"), "\0hello not allowed as file name");
	assertNot(FileAPI.isValidFileName("hello\0"), "hello\0 not allowed as file name");
}

// Filepath Test
{
	assert(FileAPI.isValidPathName("hello"), "hello as file name");
	assert(FileAPI.isValidPathName("Hello"), "Hello as file name");
	assert(FileAPI.isValidPathName("H3llo"), "H3llo as file name");
	assert(FileAPI.isValidPathName("12345"), "12345 as file name");
	assert(FileAPI.isValidPathName("testing.js"), "testing.js as file name");
	assert(FileAPI.isValidPathName("maggie"), "maggie as file name");
	assert(FileAPI.isValidPathName(".git"), ".git as file name");
	assert(FileAPI.isValidPathName("."), ". not allowed as file name");
	assert(FileAPI.isValidPathName(".a"), ".a as file name");
	assert(FileAPI.isValidPathName("a."), "a. as file name");
	assert(FileAPI.isValidPathName(".."), ".. not allowed as file name");
	assert(FileAPI.isValidPathName("..a"), "..a as file name");
	assert(FileAPI.isValidPathName("a."), "a.. as file name");
	assert(FileAPI.isValidPathName("/hello"), "/hello not allowed as file name");
	assert(FileAPI.isValidPathName("\\hello"), "\\hello not allowed as file name");
	assertNot(FileAPI.isValidPathName("\0hello"), "\0hello not allowed as file name");
	assertNot(FileAPI.isValidPathName("hello\0"), "hello\0 not allowed as file name");
}

// FileSystem Test
{
	var memory = new FileAPI.MemoryPlatform();
	var bigFileContent = "";
	for (var i = 0; i < 5000; i++) {
		bigFileContent += i;
	}
	FileAPI.LogChanges(memory, document.getElementById("log"));
	FileAPI.ShowDirectory(memory, document.getElementById("view"));
	FileAPI.Controls(memory, document.getElementById("controls"));

	setTimeout(
		function () {
			var timing = Date.now();
			console.log("Start Testing FileSystem");

			assert(memory.isSupported(), "Platform type is not supported");

			memory.createFile("TEST DATA MATTERS", "test.txt");
			memory.createFile("WOW 2nd FILE", "/test2.txt");
			memory.createFile("3 IN A ROW", "test3.txt");

			memory.createDirectory("folder");
			memory.createDirectory("folder/nested");
			memory.createDirectory("folder/nested/more/");
			memory.createDirectory("/folder/nested/wow/");
			memory.createDirectory("/folder/nested/damm");
			memory.createDirectory("folder/nested/damm");
			memory.createDirectory("/folder/second");
			memory.createDirectory("/folder/third/");
			memory.createDirectory("/folder/last");

			memory.createFile("HOT DAMM", "folder/test.txt");
			memory.createFile("NEST", "folder/nested/more/test.txt");
			memory.createFile("HIDDEN FILE", "/folder/nested/damm/test3.txt");
			memory.createFile(bigFileContent, "/blob.bin");

			var bigFile = memory.openFile("blob.bin");
			assert(bigFile instanceof FileAPI.File, "Big File (`blob.bin`) is not a file");
			assert(bigFile.isFile(), "Big File (`blob.bin`) is not a file");
			assert(!bigFile.isDirectory(), "Big File (`blob.bin`) is not a file");
			assert(bigFile.data === bigFileContent, "Big File content changed");

			bigFile.copy(memory, memory, "blob2.bin");
			var bigFile2 = memory.openFile("blob2.bin");
			assert(bigFile2 instanceof FileAPI.File, "Big File2 (`blob2.bin`) is not a file");
			assert(bigFile2.data === bigFile.data, "Big File2 content changed");

			bigFile.data = "00000000" + bigFile.data;
			bigFile.save(memory);
			bigFile = memory.openFile("blob.bin");
			assertNot(bigFile.data === bigFileContent, "Big File should be changed");

			bigFile2.remove(memory);
			assertNot(memory.hasFile("blob2.bin"), "Blob2 should be removed");

			memory.copyFile("test.txt", "test (1).txt");
			memory.copyFile(memory.openFile("test.txt"), "test (2).txt");

			assert(memory.openFile("test (1).txt").isFile(), "Copy 1 failded");
			assert(memory.openFile("test (2).txt").isFile(), "Copy 2 failded");

			assertFail(function () {
				memory.openDirectory("test.txt");
			}, "Tried to read Directory while its a File");
			assertFail(function () {
				memory.openFile("folder/");
			}, "Tried to read File while its a Directory");

			assertFail(function () {
				memory.removeDirectory("folder/nested");
			}, "Tried to remove non-empty directory");
			assertTry(function () {
				memory.removeDirectory("folder/nested", true);
			}, "Tried to remove non-empty directory RECURSIVE");

			var duration = Date.now() - timing;
			console.log("Testing done. Timing: " + duration + "ms")
		},
		1000);
}